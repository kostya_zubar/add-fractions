package main

import (
	"fmt"
	"log"
)

func main() {
	first, err := inputFraction()
	if err != nil {
		log.Fatal(err)
	}
	if !IsValidDenominator(first[1]) {
		log.Fatal("denominator can't be 0")
	}
	if !IsPositiveNumbers(first) {
		log.Fatal("numbers should be positive")
	}
	second, err := inputFraction()
	if err != nil {
		log.Fatal(err)
	}
	if !IsValidDenominator(second[1]) {
		log.Fatal("denominator can't be 0")
	}
	if !IsPositiveNumbers(second) {
		log.Fatal("numbers should be positive")
	}

	add := AddFractions(first, second)
	fmt.Printf("Result: %v/%v", add[0], add[1])
}

func IsValidDenominator(d int) bool {
	return d != 0
}

func IsPositiveNumbers(n []int) bool {
	for _, v := range n {
		if v < 0 {
			return false
		}
	}
	return true
}

func AddFractions(first, second []int) []int {
	result := make([]int, 0, 2)
	if first[1] != second[1] {
		nok := GetNok(first[1], second[1])
		result = append(result, first[0] * (nok / first[1]) + second[0] * (nok / second[1]))
		result = append(result, nok)
		return result
	}
	result = append(result, first[0] + second[0])
	result = append(result, first[1])
	return result
}

// Наименьшее общее кратное
func GetNok(a, b int) int {
	nok := a * b / GetNod(a, b)
	return nok
}

// Наибольший общий делитель
func GetNod(a, b int) int {
	for a != b {
		if a > b {
			a -= b
		} else {
			b -= a
		}
	}
	return a
}

func inputFraction() ([]int, error) {
	f :=  make([]int, 2)
	fmt.Print("Enter fraction (format 1/2): ")
	_, err := fmt.Scanf("%v/%v", &f[0], &f[1])
	if err != nil {
		return f, err
	}
	return f, nil
}

