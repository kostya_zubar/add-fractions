package main

import "testing"

func TestIsValidDenominator(t *testing.T) {
	b := IsValidDenominator(1)
	if !b {
		t.Errorf("expect true, got %t", b)
	}
}

func TestIsNotValidDenominator(t *testing.T) {
	b := IsValidDenominator(0)
	if b {
		t.Errorf("expect false, got %t", b)
	}
}

func TestIsPositiveNumbers(t *testing.T) {
	n := []int{1, 3}
	b := IsPositiveNumbers(n)
	if !b {
		t.Errorf("numbers: %v, expect true, got %t", n, b)
	}
}
func TestIsNotPositiveNumbers(t *testing.T) {
	n := []int{-11, -2}
	b := IsPositiveNumbers(n)
	if b {
		t.Errorf("numbers: %v, expect false, got %t", n, b)
	}
}

func TestGetNod(t *testing.T) {
	a := 4
	b := 16
	r := 4
	nod := GetNod(a, b)
	if nod != r {
		t.Errorf("nod of %v and %v; expect %v, got %v", a, b, r, nod)
	}
}

func TestGetNoc(t *testing.T) {
	a := 3
	b := 4
	r := 12
	nok := GetNok(a, b)
	if nok != r {
		t.Errorf("nok of %v and %v; expect %v, got %v", a, b, r, nok)
	}
}

func TestAddFractions(t *testing.T) {
	a := []int{1, 3}
	b := []int{4, 4}
	r := []int{16, 12}
	result := AddFractions(a, b)
	if result[0] != r[0] || result[1] != r[1] {
		t.Errorf("AddFractions of %v and %v; expect %v, got %v", a, b, r, result)
	}
}

func TestAddFractionsEqualDenominator(t *testing.T) {
	a := []int{2, 7}
	b := []int{1, 7}
	r := []int{3, 7}
	result := AddFractions(a, b)
	if result[0] != r[0] || result[1] != r[1] {
		t.Errorf("AddFractions of %v and %v; expect %v, got %v", a, b, r, result)
	}
}
